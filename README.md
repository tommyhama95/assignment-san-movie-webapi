# Noroff Accelerate ASP.NET Core Assignment 3 - Web API
## Setup
In order to setup the project you first need to add your own SqlConnectionString and switch it out in the startup.cs file. 

First in the appsettings.json file, make a custom ConnectionString following the Name + Connection naming convention. Ex: TommyConnection.

The add the following:
- Initial Catalog= CinemaDTO
- Integrated Security= True

Final string should look like this:

"Data Source= {your string}; Initial Catalog= CinemaDTO; Integrated Security= True;"


### **Developer Team:**
-  Krister (SeventhDisaster) Emanuelsen
- Tommy (TommyHama95) Hamarsnes

## Appendix A:
Following the Business rules to create the Database diagram the following UML Diagram using Crows notation has been created through Lucidchart:

![Database Diagram with Crow's Notation](./diagram.png)
(Not through SSMS because of Authentication issues).

*OBS: Some VARCHAR lengths might exaggerate a bit*

### **Seeding**
In the CinemaDBContext.cs you will find seeding for the database with movie data.

## Appendix B:
Contents of Appendix B can be found pretty much anywhere inside the project.




## Copyright strike prevention
Franchises, movies and characters are completely and utterly original and totally do not infringe on any copyright whatsoever!
Trust us, we are not lawyers.
> See our markup below to find legal justification for our existing data:

- Lord of the Rings -> Lord of the Buckets
- Demon Slayer -> Demon Slapper
- Pokémon -> Pokemon

See? No problem