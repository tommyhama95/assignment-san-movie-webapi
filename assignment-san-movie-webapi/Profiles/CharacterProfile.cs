﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using assignment_san_movie_webapi.Data.DTO.CharacterDTO;
using assignment_san_movie_webapi.Data.Models;
using AutoMapper;

namespace assignment_san_movie_webapi.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile ()
        {
            CreateMap<Character, CharacterReadDTO>()
                .ForMember(cdto => cdto.Movies, opt => opt
                .MapFrom(m => m.Movies.Select(m => m.Id).ToArray()));
            CreateMap<CharacterCreateDTO, Character>();
            CreateMap<CharacterEditDTO, Character>();
        }
    }
}
