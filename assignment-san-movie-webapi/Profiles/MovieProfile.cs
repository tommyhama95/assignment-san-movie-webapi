﻿using System;
using System.Collections.Generic;
using AutoMapper;
using System.Linq;
using System.Threading.Tasks;
using assignment_san_movie_webapi.Data.Models;
using assignment_san_movie_webapi.Data.DTO;

namespace assignment_san_movie_webapi.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieReadDTO>()
                .ForMember(mdto => mdto.Characters, opt => opt
                .MapFrom(m => m.Characters.Select(c => c.Id).ToArray()))
                .ForMember(mdto => mdto.FranchiseId, opt => opt
                .MapFrom(f => f.FranchiseId))
                .ReverseMap();
            CreateMap<MovieCreateDTO, Movie>()
                .ForMember(m => m.FranchiseId, opt => opt
                .MapFrom(mdto => mdto.FranchiseId));

            CreateMap<MovieEditDTO, Movie>()
                .ForMember(m => m.FranchiseId, opt => opt
                .MapFrom(mdto => mdto.FranchiseId));

        }
    }
}
