﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using assignment_san_movie_webapi.Data.DTO.FranchiseDTO;
using assignment_san_movie_webapi.Data.Models;
using AutoMapper;

namespace assignment_san_movie_webapi.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseReadDTO>()
                .ForMember(fdto => fdto.Movies, opt => opt
                .MapFrom(f => f.Movies.Select(m => m.Id).ToArray()))
                .ReverseMap();
            CreateMap<FranchiseCreateDTO, Franchise>();
            CreateMap<FranchiseEditDTO, Franchise>();
        }
    }
}
