﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using assignment_san_movie_webapi.Data.Models;
using assignment_san_movie_webapi.Services;
using AutoMapper;
using assignment_san_movie_webapi.Data.DTO.CharacterDTO;

namespace assignment_san_movie_webapi.Controllers
{
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Route("api/[controller]")]
    [ApiController]
    public class CharactersController : ControllerBase
    {
        private readonly ICharacterService _service;
        private readonly IMapper _mapper;

        /// <summary>
        /// Instances the controller with an automapper for DTOs and a service
        /// </summary>
        /// <param name="mapper"></param>
        /// <param name="service"></param>
        public CharactersController(IMapper mapper, ICharacterService service)
        {
            _mapper = mapper;
            _service = service;
        }

        /// <summary>
        /// Gets all characters
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters()
        {
            return _mapper.Map<List<CharacterReadDTO>>(await _service.GetAllCharactersAsync());
        }

        /// <summary>
        /// Gets all characters by ID
        /// </summary>
        /// <param name="id">Character ID</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int id)
        {
            var character = await _service.GetCharacterByIdAsync(id);

            if (character == null)
            {
                return NotFound();
            }

            return _mapper.Map<CharacterReadDTO>(character);
        }

        /// <summary>
        /// Edits the details of a character found by ID
        /// </summary>
        /// <param name="id">Character ID</param>
        /// <param name="characterDTO">Character object to replace with</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, CharacterEditDTO characterDTO)
        {
            if (id != characterDTO.Id)
            {
                return BadRequest();
            }

            try
            {
                Character domainCharacter = _mapper.Map<Character>(characterDTO);
                await _service.UpdateCharacterAsync(domainCharacter);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Creates a new character from a character object
        /// </summary>
        /// <param name="characterDTO">Character object</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<CharacterReadDTO>> PostCharacter(CharacterCreateDTO characterDTO)
        {
            Character domainCharacter = _mapper.Map<Character>(characterDTO);
            await _service.AddCharacterAsync(domainCharacter);

            return CreatedAtAction("GetCharacter", 
                new { id = domainCharacter.Id }, 
                _mapper.Map<CharacterReadDTO>(domainCharacter));
        }

        /// <summary>
        /// Deletes a character by ID
        /// </summary>
        /// <param name="id">Character ID</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            if (!CharacterExists(id))
            {
                return NotFound();
            }
            await _service.DeleteCharacter(id);
            return NoContent();
        }

        /// <summary>
        /// Checks if the character exists
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool CharacterExists(int id)
        {
            return _service.CharacterExists(id);
        }
    }
}
