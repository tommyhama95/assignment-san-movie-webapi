﻿using System;
using System.Collections.Generic;
using AutoMapper;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using assignment_san_movie_webapi.Data.Models;
using assignment_san_movie_webapi.Services;
using assignment_san_movie_webapi.Data.DTO;
using assignment_san_movie_webapi.Data.DTO.CharacterDTO;

namespace assignment_san_movie_webapi.Controllers
{
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IMovieService _service;

        /// <summary>
        /// Instances the controller with an automapper for DTOs and a service
        /// </summary>
        /// <param name="mapper"></param>
        /// <param name="movieService"></param>
        public MoviesController(IMapper mapper, IMovieService movieService) {
            _mapper = mapper;
            _service = movieService;
        }

        /// <summary>
        /// Gets all movies
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
        {
            return _mapper.Map<List<MovieReadDTO>>(await _service.GetAllMoviesAsync());
        }

        /// <summary>
        /// Gets a movie by ID
        /// </summary>
        /// <param name="id">Movie ID</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
        {
            if (!_service.MovieExists(id))
            {
                return NotFound();
            }

            return _mapper.Map<MovieReadDTO>(await _service.GetMovieByIdAsync(id));
        }

        /// <summary>
        /// Gets all characters in a movie
        /// </summary>
        /// <param name="id">Movie ID</param>
        /// <returns></returns>
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharactersByMovie(int id)
        {
            return _mapper.Map<List<CharacterReadDTO>>(await _service.GetAllCharactersInMovie(id));
        }

        /// <summary>
        /// Edits a movie based on an ID and a movie object
        /// </summary>
        /// <param name="id">Movie ID</param>
        /// <param name="dtoMovie">Movie object</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, MovieEditDTO dtoMovie)
        {
            if (id != dtoMovie.Id)
            {
                return BadRequest();
            }

            if(!MovieExists(id))
            {
                return NotFound();
            }
            try
            {
                Movie movie = _mapper.Map<Movie>(dtoMovie);
                await _service.UpdateMovieAsync(movie);
            } catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return NoContent();
        }

        /// <summary>
        /// Edits the characters in a movie based on id, populated based on a list of character IDs
        /// </summary>
        /// <param name="id">Movie ID</param>
        /// <param name="characters">List of character IDs</param>
        /// <returns></returns>
        [HttpPut("{id}/characters")]
        public async Task<IActionResult> UpdateMovieCharacters(int id, List<int> characters)
        {
            if(!_service.MovieExists(id))
            {
                return NotFound();
            }

            try
            {
                await _service.UpdateMovieCharactersAsync(id, characters);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest();
            }

            return NoContent();
        }

        /// <summary>
        /// Creates a movie based on a movie object
        /// </summary>
        /// <param name="dtoMovie">Movie object</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Movie>> PostMovie(MovieCreateDTO dtoMovie)
        {
            Movie domainMovie = _mapper.Map<Movie>(dtoMovie);
            domainMovie = await _service.AddMovieAsync(domainMovie);
            

            return CreatedAtAction("GetMovie", 
                new { id = domainMovie.Id }, 
                _mapper.Map<MovieReadDTO>(domainMovie)
            );
        }

        /// <summary>
        /// Deletes a movie based on a movie ID
        /// </summary>
        /// <param name="id">Movie ID</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            if (!MovieExists(id))
            {
                return NotFound();
            }

            await _service.DeleteMovieAsync(id);

            return NoContent();
        }
        
        /// <summary>
        /// Checks if a movie exists
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool MovieExists(int id)
        {
            return _service.MovieExists(id); ;
        }
    }
}
