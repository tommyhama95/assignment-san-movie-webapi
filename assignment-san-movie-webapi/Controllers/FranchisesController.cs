﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using assignment_san_movie_webapi.Data.Models;
using AutoMapper;
using assignment_san_movie_webapi.Data.DTO.FranchiseDTO;
using assignment_san_movie_webapi.Services;
using assignment_san_movie_webapi.Data.DTO;
using assignment_san_movie_webapi.Data.DTO.CharacterDTO;

namespace assignment_san_movie_webapi.Controllers
{
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Route("api/[controller]")]
    [ApiController]
    public class FranchisesController : ControllerBase
    {
        private readonly IFranchiseService _service;
        private readonly IMapper _mapper;

        /// <summary>
        /// Instances the controller with an automapper for DTOs and a service
        /// </summary>
        /// <param name="mapper"></param>
        /// <param name="service"></param>
        public FranchisesController(IMapper mapper, IFranchiseService service)
        {
            _mapper = mapper;
            _service = service;
        }

        /// <summary>
        /// Gets all franchises
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            return _mapper.Map<List<FranchiseReadDTO>>(await _service.GetAllFranchisesAsync());
        }

        /// <summary>
        /// Gets a franchise by ID
        /// </summary>
        /// <param name="id">Franchise ID</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            var franchise = await _service.GetFranchiseByIdAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<FranchiseReadDTO>(franchise);
        }

        /// <summary>
        /// Gets all movies in a franchise
        /// </summary>
        /// <param name="id">Franchise ID</param>
        /// <returns></returns>
        [HttpGet("{id}/movies")]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMoviesInFranchise(int id)
        {
            return _mapper.Map<List<MovieReadDTO>>(await _service.GetAllMoviesInFranchiseAsync(id));
        }
        
        /// <summary>
        /// Gets all unique characters within a franchise
        /// </summary>
        /// <param name="id">Franchise ID</param>
        /// <returns></returns>
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharactersInFranchise(int id)
        {
            return _mapper.Map<List<CharacterReadDTO>>(await _service.GetAllCharactersInFranchiseAsync(id));
        }

        /// <summary>
        /// Edits a franchise based on ID and a franchise object
        /// </summary>
        /// <param name="id">Franchise ID</param>
        /// <param name="franchiseDTO">Franchise object</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchiseEditDTO franchiseDTO)
        {
            if (id != franchiseDTO.Id)
            {
                return BadRequest();
            }

            try
            {
                Franchise domainFranchise = _mapper.Map<Franchise>(franchiseDTO);
                await _service.UpdateFranchiseAsync(domainFranchise);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Creates a franchise based on a franchise object
        /// </summary>
        /// <param name="franchiseDTO">Franchise object</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<FranchiseReadDTO>> PostFranchise(FranchiseCreateDTO franchiseDTO)
        {
            Franchise domainFranchise = _mapper.Map<Franchise>(franchiseDTO);
            await _service.AddFranchiseAsync(domainFranchise);

            return CreatedAtAction("GetFranchise",
                new { id = domainFranchise.Id },
                _mapper.Map<FranchiseReadDTO>(domainFranchise)) ;
        }

        /// <summary>
        /// Deletes a franchise based on id
        /// </summary>
        /// <param name="id">Franchise ID</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            if (!FranchiseExists(id))
            {
                return NotFound();
            }

            await _service.DeleteFranchiseAsync(id);
            return NoContent();
        }

        /// <summary>
        /// Checks if a franchise exists
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool FranchiseExists(int id)
        {
            return _service.FranchiseExists(id);
        }
    }
}
