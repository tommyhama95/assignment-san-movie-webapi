﻿using assignment_san_movie_webapi.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace assignment_san_movie_webapi.Services
{
    public interface IMovieService
    {
        public Task<IEnumerable<Movie>> GetAllMoviesAsync();
        public Task<IEnumerable<Character>> GetAllCharactersInMovie(int movieId);
        public Task<Movie> GetMovieByIdAsync(int id);
        public Task<Movie> AddMovieAsync(Movie movie);
        public Task UpdateMovieAsync(Movie movie);
        public Task UpdateMovieCharactersAsync(int movieId, List<int> characters);
        public Task DeleteMovieAsync(int id);
        public bool MovieExists(int id);
    }
}
