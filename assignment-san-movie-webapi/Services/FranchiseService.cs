﻿using assignment_san_movie_webapi.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace assignment_san_movie_webapi.Services
{
    public class FranchiseService : IFranchiseService
    {
        private readonly CinemaDbContext _context;

        public FranchiseService(CinemaDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Creates a new franchise in the database based on a franchise object
        /// </summary>
        /// <param name="franchise">Franchise object</param>
        /// <returns></returns>
        public async Task<Franchise> AddFranchiseAsync(Franchise franchise)
        {
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();
            return franchise;
        }

        /// <summary>
        /// Deletes a franchise from the database based on a franchise ID
        /// </summary>
        /// <param name="id">Franchise ID</param>
        /// <returns></returns>
        public async Task DeleteFranchiseAsync(int id)
        {
            Franchise franchise = await _context.FindAsync<Franchise>(id);
            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Checks if a franchise exists in database based on ID
        /// </summary>
        /// <param name="id">Franchise ID</param>
        /// <returns></returns>
        public bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(f => f.Id == id);
        }

        /// <summary>
        /// Gets all unique characters from a franchise based on franchiseID in the database
        /// </summary>
        /// <param name="franchiseId">Franchise ID</param>
        /// <returns></returns>
        public async Task<IEnumerable<Character>> GetAllCharactersInFranchiseAsync(int franchiseId)
        {
            List<Movie> moviesInFranchise = await _context.Movies
                .Select(m => m)
                .Include(m => m.Characters)
                .Where(m => m.FranchiseId == franchiseId)
                .ToListAsync();
            HashSet<Character> charactersInMovies = new();

            foreach(Movie movie in moviesInFranchise)
            {
                charactersInMovies.UnionWith(movie.Characters);
            }

            return charactersInMovies;
        }

        /// <summary>
        /// Gets all franchisese from database
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Franchise>> GetAllFranchisesAsync()
        {
            return await _context.Franchises.Include(f => f.Movies).ToListAsync();
        }

        /// <summary>
        /// Gets all the movies within a franchise from the database based on franchiseID
        /// </summary>
        /// <param name="franchiseId">Franchise ID</param>
        /// <returns></returns>
        public async Task<IEnumerable<Movie>> GetAllMoviesInFranchiseAsync(int franchiseId)
        {
            return await _context.Movies.Select(m => m)
                .Where(m => m.FranchiseId == franchiseId)
                .Include(m => m.Characters)
                .ToListAsync();
        }

        /// <summary>
        /// Gets a specific franchise from the database based on ID
        /// </summary>
        /// <param name="id">Franchise ID</param>
        /// <returns></returns>
        public async Task<Franchise> GetFranchiseByIdAsync(int id)
        {
            return await _context.FindAsync<Franchise>(id);
        }

        /// <summary>
        /// Updates a franchise in the database based on a franchise object
        /// </summary>
        /// <param name="franchise">Franchise object</param>
        /// <returns></returns>
        public async Task UpdateFranchiseAsync(Franchise franchise)
        {
            _context.Entry<Franchise>(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Updates the movies in a franchise based on a franchise ID and a list of movie ID's to set
        /// </summary>
        /// <param name="id">Franchise IDs</param>
        /// <param name="movies">List of movie IDs</param>
        /// <returns></returns>
        public async Task UpdateFranchiseMoviesAsync(int id, List<int> movies)
        {
            Franchise franchiseToUpdate = await _context.Franchises
                .Include(f => f.Movies)
                .Where(f => f.Id == id)
                .FirstAsync();

            List<Movie> movieList = new();
            foreach (int movieId in movies)
            {
                Movie m = await _context.Movies.FindAsync(movieId);
                if (m == null)
                    throw new KeyNotFoundException();
                movieList.Add(m);
            }
            franchiseToUpdate.Movies = movieList;
            await _context.SaveChangesAsync();
        }
    }
}
