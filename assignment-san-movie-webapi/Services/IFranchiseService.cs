﻿using assignment_san_movie_webapi.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace assignment_san_movie_webapi.Services
{
    public interface IFranchiseService
    {
        public Task<IEnumerable<Franchise>> GetAllFranchisesAsync();
        public Task<IEnumerable<Character>> GetAllCharactersInFranchiseAsync(int franchiseId);
        public Task<IEnumerable<Movie>> GetAllMoviesInFranchiseAsync(int franchiseId);
        public Task<Franchise> GetFranchiseByIdAsync(int id);
        public Task<Franchise> AddFranchiseAsync(Franchise franchise);
        public Task UpdateFranchiseAsync(Franchise franchise);
        public Task UpdateFranchiseMoviesAsync(int id, List<int> movies);
        public Task DeleteFranchiseAsync(int id);
        public bool FranchiseExists(int id);

    }
}
