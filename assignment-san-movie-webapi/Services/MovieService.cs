﻿using assignment_san_movie_webapi.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace assignment_san_movie_webapi.Services
{
    public class MovieService : IMovieService
    {
        private readonly CinemaDbContext _context;

        public MovieService(CinemaDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Creates a new movie in database based on a movie object
        /// </summary>
        /// <param name="movie">Movie object</param>
        /// <returns></returns>
        public async Task<Movie> AddMovieAsync(Movie movie)
        {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();

            return movie;
        }

        /// <summary>
        /// Deletes a movie in the database based on a movie ID
        /// </summary>
        /// <param name="id">Movie Id</param>
        /// <returns></returns>
        public async Task DeleteMovieAsync(int id)
        {
            Movie movie = await _context.FindAsync<Movie>(id);
            _context.Movies.Remove(movie);
           await _context.SaveChangesAsync(); ;
        }

        /// <summary>
        /// Gets all characters in a movie from the database based on a movie iD
        /// </summary>
        /// <param name="id">Movie ID</param>
        /// <returns></returns>
        public async Task<IEnumerable<Character>> GetAllCharactersInMovie(int id)
        {
            Movie movie = await _context.Movies
                .Include(m => m.Characters)
                .Where(m => m.Id == id)
                .FirstOrDefaultAsync();
            return movie.Characters;
        }

        /// <summary>
        /// Yeah you know the drill. It goes and gets all the smelly movies. All of them.
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Movie>> GetAllMoviesAsync()
        {
            return await _context.Movies
                .Include(c => c.Characters)
                .Include(f => f.Franchise)
                .ToListAsync();
        }

        /// <summary>
        /// Gets a specific movie from the database by movieID
        /// </summary>
        /// <param name="id">Movie ID</param>
        /// <returns></returns>
        public async Task<Movie> GetMovieByIdAsync(int id)
        {
            return await _context.Movies.FindAsync(id);
        }

        /// <summary>
        /// Checks if a movie exists in the database
        /// </summary>
        /// <param name="id">Movie ID</param>
        /// <returns></returns>
        public bool MovieExists(int id)
        {
            return _context.Movies.Any(a => a.Id == id);
        }

        /// <summary>
        /// Updates a movie in the database based on a movie object
        /// </summary>
        /// <param name="movie">Movie object</param>
        /// <returns></returns>
        public async Task UpdateMovieAsync(Movie movie)
        {
            _context.Entry(movie).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Updates characters in a movie in the database based on a provided list of character ID's
        /// </summary>
        /// <param name="movieId">Movie ID</param>
        /// <param name="characters">List of Character IDs</param>
        /// <returns></returns>
        public async Task UpdateMovieCharactersAsync(int movieId, List<int> characters)
        {
            Movie characterUpdateTarget = await _context.Movies
                .Include(m => m.Characters)
                .Where(m => m.Id == movieId)
                .FirstAsync();

            List<Character> chars = new();
            foreach(int characterId in characters)
            {
                Character chara = await _context.Characters.FindAsync(characterId);
                if(chara == null)
                {
                    throw new KeyNotFoundException();
                }
                chars.Add(chara);
            }
            characterUpdateTarget.Characters = chars;
            await _context.SaveChangesAsync();
        }
    }
}
