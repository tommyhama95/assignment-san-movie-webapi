﻿using assignment_san_movie_webapi.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace assignment_san_movie_webapi.Services
{
    public class CharacterService : ICharacterService
    {

        public readonly CinemaDbContext _context;

        public CharacterService(CinemaDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Serializes a new character to the database
        /// </summary>
        /// <param name="character">Character object</param>
        /// <returns></returns>
        public async Task<Character> AddCharacterAsync(Character character)
        {
            _context.Add(character);
            await _context.SaveChangesAsync();
            return character;
        }

        /// <summary>
        /// Check if a character exists in database based on id
        /// </summary>
        /// <param name="id">character id</param>
        /// <returns></returns>
        public bool CharacterExists(int id)
        {
            return _context.Characters.Any(c => c.Id == id);
        }

        /// <summary>
        /// Deletes a character from the database based on id
        /// </summary>
        /// <param name="id">character ID</param>
        /// <returns></returns>
        public async Task DeleteCharacter(int id)
        {
            var character = await _context.FindAsync<Character>(id);
            _context.Remove(character);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Selects all characters from the database with movies
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Character>> GetAllCharactersAsync()
        {
            return await _context.Characters.Include(c => c.Movies).ToListAsync();
        }

        /// <summary>
        /// Selects a character from the database based on a character ID
        /// </summary>
        /// <param name="id">Character ID</param>
        /// <returns></returns>
        public async Task<Character> GetCharacterByIdAsync(int id)
        {
            return await _context.FindAsync<Character>(id);
        }

        /// <summary>
        /// Edits a character in the database based on a character object
        /// </summary>
        /// <param name="character">Character object</param>
        /// <returns></returns>
        public async Task UpdateCharacterAsync(Character character)
        {
            _context.Entry<Character>(character).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}
