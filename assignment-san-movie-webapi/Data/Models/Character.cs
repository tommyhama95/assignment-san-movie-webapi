﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace assignment_san_movie_webapi.Data.Models
{
    [Table("Characters")]
    public class Character
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(100)]
        public string FullName { get; set; }
        [MaxLength(100)]
        public string? Alias { get; set; }
        public bool IsMale { get; set; }
        [MaxLength(500)]
        public string Picture { get; set; }

        public ICollection<Movie> Movies { get; set; }
    }
}
