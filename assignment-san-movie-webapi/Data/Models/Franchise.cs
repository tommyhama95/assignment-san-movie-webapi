﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace assignment_san_movie_webapi.Data.Models
{
    [Table("Franchises")]
    public class Franchise
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(1000)]
        public string Name { get; set; }

        [MaxLength(10000)]
        public string Description { get; set; }

        public ICollection<Movie> Movies { get; set; }
    }
}
