﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace assignment_san_movie_webapi.Data.Models
{
    [Table("Movies")]
    public class Movie
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(500)]
        public string Title { get; set; }
        [Required]
        [MaxLength(1000)]
        public string Genre { get; set; }
        [Required]
        public int ReleaseYear { get; set; }
        [MaxLength(100)]
        public string Director { get; set; }
        [MaxLength(500)]
        public string Picture { get; set; }
        [MaxLength(100)]
        public string Trailer { get; set; }
        public ICollection<Character> Characters { get; set; }
        public int FranchiseId { get; set; }
        public Franchise Franchise { get; set; }
    }
}
