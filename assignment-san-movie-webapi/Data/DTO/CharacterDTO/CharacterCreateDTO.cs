﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace assignment_san_movie_webapi.Data.DTO.CharacterDTO
{
    public class CharacterCreateDTO
    {
        public string FullName { get; set; }
        public string Alias { get; set; }
        public bool IsMale { get; set; }
        public string Picture { get; set; }
    }
}
