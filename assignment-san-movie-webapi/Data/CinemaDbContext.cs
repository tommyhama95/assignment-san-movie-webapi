﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace assignment_san_movie_webapi.Data.Models
{
    public class CinemaDbContext : DbContext
    {
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Character> Characters { get; set; }
        public DbSet<Franchise> Franchises { get; set; }

        public CinemaDbContext([NotNullAttribute] DbContextOptions options) : base(options)
        {
        }

        /// <summary>
        /// Seeds the batadase with some dase content
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Movie>().HasData(new Movie { 
                Id = 1, 
                Title = "Lord of the Buckets", 
                Genre = "Horror, Thriller, Action, Comedy, Sci-Fi", 
                ReleaseYear = 2069, 
                Director = "Soldier of Teleporting Bread", 
                Picture = @"https://pbs.twimg.com/profile_images/1302192836394266624/EmeqPeG2.jpg", 
                Trailer = @"https://www.youtube.com/watch?v=S2zfi4hAwSA", 
                FranchiseId = 1
            });
            modelBuilder.Entity<Movie>().HasData(new Movie { 
                Id = 2, 
                Title = "Demon Slapper - The Movie: Finite Tram", 
                Genre = "Adventure, Dark fantasy, Martial Arts", 
                ReleaseYear = 2020, 
                Director = "Haruo Sotozaki", 
                Picture = @"https://upload.wikimedia.org/wikipedia/en/2/21/Kimetsu_no_Yaiba_Mugen_Ressha_Hen_Poster.jpg", 
                Trailer = @"https://www.youtube.com/watch?v=ATJYac_dORw", 
                FranchiseId = 2
            });
            modelBuilder.Entity<Movie>().HasData(new Movie { 
                Id = 3, 
                Title = "Pokemon: Giratina & The Sky Warrior", 
                Genre = "Adventure, Sci-Fi, Animation", 
                ReleaseYear = 2008, 
                Director = "Kunihiko Yuyama", 
                Picture = @"https://upload.wikimedia.org/wikipedia/en/2/20/Giratina_%26_The_Sky_Warrior_Japanese_Poster.jpg", 
                Trailer = @"https://www.youtube.com/watch?v=7DliP7EX9To", 
                FranchiseId = 3
            });            
            modelBuilder.Entity<Movie>().HasData(new Movie { 
                Id = 4, 
                Title = "Pokemon: The Rise of Darkrai", 
                Genre = "Adventure, Sci-Fi, Animation", 
                ReleaseYear = 2007, 
                Director = "Kunihiko Yuyama", 
                Picture = @"https://upload.wikimedia.org/wikipedia/en/c/c4/The_Rise_of_Darkrai_2.JPG", 
                Trailer = @"https://www.youtube.com/watch?v=koKPESdDzYU", 
                FranchiseId = 3
            });

            modelBuilder.Entity<Franchise>().HasData(new Franchise {Id = 1, Name = "Team Fortress", Description = "A bunch of idiots teleporting bread and loves a bucket" });
            modelBuilder.Entity<Franchise>().HasData(new Franchise {Id = 2, Name = "Demon Slapper", Description = "People slapping demons" });
            modelBuilder.Entity<Franchise>().HasData(new Franchise {Id = 3, Name = "Pokemon", Description = "Fictional creature cruelty" });

            // Team Fortress
            modelBuilder.Entity<Character>().HasData(new Character {
                Id = 1,
                FullName = "Scout",
                IsMale = true,
                Picture = @"https://www.pngkit.com/png/detail/356-3566857_datsecksybois-journal-deviantart-tf2-scout-face.png"
            });
            // Demon Slapper
            modelBuilder.Entity<Character>().HasData(new Character {
                Id = 2,
                FullName = "Kamado Tanjiro",
                Alias = "Kamaboko Gonpachiro",
                IsMale = true,
                Picture = @"https://static.wikia.nocookie.net/kimetsu-no-yaiba/images/c/c7/Tanjiro_anime_design.png/revision/latest?cb=20181128204204"
            });
            // Pokemon
            modelBuilder.Entity<Character>().HasData(new Character {
                Id = 3,
                FullName = "Dawn",
                IsMale = false,
                Picture = @"https://cdn2.bulbagarden.net/upload/d/dc/Brilliant_Diamond_Shining_Pearl_Dawn.png"
            });       
            // Pokemon
            modelBuilder.Entity<Character>().HasData(new Character {
                Id = 4,
                FullName = "Brook",
                Alias = "Pewter City Gym Leader",
                IsMale = true,
                Picture = @"https://static3.cbrimages.com/wordpress/wp-content/uploads/2020/11/Pokemon-Brock.jpg?q=50&fit=crop&w=960&h=500&dpr=1.5"
            });
            // Demon Slapper
            modelBuilder.Entity<Character>().HasData(new Character {
                Id = 5,
                FullName = "Agatsuma Zenitsu",
                Alias = "Monitsu",
                IsMale = true,
                Picture = @"https://static.wikia.nocookie.net/kimetsu-no-yaiba/images/e/e0/Zenitsu_anime_design.png/revision/latest/scale-to-width-down/700?cb=20181128204231"
            });
            // Demon Slapper
            modelBuilder.Entity<Character>().HasData(new Character {
                Id = 6,
                FullName = "Hashibira Inosuke",
                Alias = "Inoko",
                IsMale = true,
                Picture = @" https://static.wikia.nocookie.net/kimetsu-no-yaiba/images/9/94/Inosuke_anime_design.png/revision/latest/scale-to-width-down/700?cb=20181128204238"
            });
            // Demon Slapper
            modelBuilder.Entity<Character>().HasData(new Character {
                Id = 7,
                FullName = "Kamado Nezuko",
                Alias = "Chosen Demon",
                IsMale = false,
                Picture = @"https://static.wikia.nocookie.net/kimetsu-no-yaiba/images/2/2f/Nezuko_anime_design.png/revision/latest/scale-to-width-down/700?cb=20200323234810"
            });

            // M2M linkings
            modelBuilder.Entity<Movie>()
                .HasMany(m => m.Characters)
                .WithMany(c => c.Movies)
                .UsingEntity<Dictionary<string, object>>(
                    "MovieCharacters",
                    a => a.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                    b => b.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                    f =>
                    {
                        f.HasKey("CharacterId", "MovieId");
                        f.HasData(

                                new { CharacterId = 1, MovieId = 1 },
                                new { CharacterId = 2, MovieId = 2 },
                                new { CharacterId = 3, MovieId = 3 },
                                new { CharacterId = 4, MovieId = 3 },
                                new { CharacterId = 5, MovieId = 2 },
                                new { CharacterId = 6, MovieId = 2 },
                                new { CharacterId = 7, MovieId = 2 }
                        );
                    }
                );



        }
    }
}
