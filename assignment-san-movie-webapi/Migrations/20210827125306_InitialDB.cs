﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace assignment_san_movie_webapi.Migrations
{
    public partial class InitialDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Characters",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FullName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Alias = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    IsMale = table.Column<bool>(type: "bit", nullable: false),
                    Picture = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Characters", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Franchises",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", maxLength: 10000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchises", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Movies",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: false),
                    Genre = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: false),
                    ReleaseYear = table.Column<int>(type: "int", nullable: false),
                    Director = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Picture = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    Trailer = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    FranchiseId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Movies_Franchises_FranchiseId",
                        column: x => x.FranchiseId,
                        principalTable: "Franchises",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MovieCharacters",
                columns: table => new
                {
                    CharacterId = table.Column<int>(type: "int", nullable: false),
                    MovieId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MovieCharacters", x => new { x.CharacterId, x.MovieId });
                    table.ForeignKey(
                        name: "FK_MovieCharacters_Characters_CharacterId",
                        column: x => x.CharacterId,
                        principalTable: "Characters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MovieCharacters_Movies_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Characters",
                columns: new[] { "Id", "Alias", "FullName", "IsMale", "Picture" },
                values: new object[,]
                {
                    { 1, null, "Scout", true, "https://www.pngkit.com/png/detail/356-3566857_datsecksybois-journal-deviantart-tf2-scout-face.png" },
                    { 2, "Kamaboko Gonpachiro", "Kamado Tanjiro", true, "https://static.wikia.nocookie.net/kimetsu-no-yaiba/images/c/c7/Tanjiro_anime_design.png/revision/latest?cb=20181128204204" },
                    { 3, null, "Dawn", false, "https://cdn2.bulbagarden.net/upload/d/dc/Brilliant_Diamond_Shining_Pearl_Dawn.png" },
                    { 4, "Pewter City Gym Leader", "Brook", true, "https://static3.cbrimages.com/wordpress/wp-content/uploads/2020/11/Pokemon-Brock.jpg?q=50&fit=crop&w=960&h=500&dpr=1.5" },
                    { 5, "Monitsu", "Agatsuma Zenitsu", true, "https://static.wikia.nocookie.net/kimetsu-no-yaiba/images/e/e0/Zenitsu_anime_design.png/revision/latest/scale-to-width-down/700?cb=20181128204231" },
                    { 6, "Inoko", "Hashibira Inosuke", true, " https://static.wikia.nocookie.net/kimetsu-no-yaiba/images/9/94/Inosuke_anime_design.png/revision/latest/scale-to-width-down/700?cb=20181128204238" },
                    { 7, "Chosen Demon", "Kamado Nezuko", false, "https://static.wikia.nocookie.net/kimetsu-no-yaiba/images/2/2f/Nezuko_anime_design.png/revision/latest/scale-to-width-down/700?cb=20200323234810" }
                });

            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "A bunch of idiots teleporting bread and loves a bucket", "Team Fortress" },
                    { 2, "People slapping demons", "Demon Slapper" },
                    { 3, "Fictional creature cruelty", "Pokemon" }
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "Picture", "ReleaseYear", "Title", "Trailer" },
                values: new object[,]
                {
                    { 1, "Soldier of Teleporting Bread", 1, "Horror, Thriller, Action, Comedy, Sci-Fi", "https://pbs.twimg.com/profile_images/1302192836394266624/EmeqPeG2.jpg", 2069, "Lord of the Buckets", "https://www.youtube.com/watch?v=S2zfi4hAwSA" },
                    { 2, "Haruo Sotozaki", 2, "Adventure, Dark fantasy, Martial Arts", "https://upload.wikimedia.org/wikipedia/en/2/21/Kimetsu_no_Yaiba_Mugen_Ressha_Hen_Poster.jpg", 2020, "Demon Slapper - The Movie: Finite Tram", "https://www.youtube.com/watch?v=ATJYac_dORw" },
                    { 3, "Kunihiko Yuyama", 3, "Adventure, Sci-Fi, Animation", "https://upload.wikimedia.org/wikipedia/en/2/20/Giratina_%26_The_Sky_Warrior_Japanese_Poster.jpg", 2008, "Pokemon: Giratina & The Sky Warrior", "https://www.youtube.com/watch?v=7DliP7EX9To" },
                    { 4, "Kunihiko Yuyama", 3, "Adventure, Sci-Fi, Animation", "https://upload.wikimedia.org/wikipedia/en/c/c4/The_Rise_of_Darkrai_2.JPG", 2007, "Pokemon: The Rise of Darkrai", "https://www.youtube.com/watch?v=koKPESdDzYU" }
                });

            migrationBuilder.InsertData(
                table: "MovieCharacters",
                columns: new[] { "CharacterId", "MovieId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 2, 2 },
                    { 5, 2 },
                    { 6, 2 },
                    { 7, 2 },
                    { 3, 3 },
                    { 4, 3 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_MovieCharacters_MovieId",
                table: "MovieCharacters",
                column: "MovieId");

            migrationBuilder.CreateIndex(
                name: "IX_Movies_FranchiseId",
                table: "Movies",
                column: "FranchiseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MovieCharacters");

            migrationBuilder.DropTable(
                name: "Characters");

            migrationBuilder.DropTable(
                name: "Movies");

            migrationBuilder.DropTable(
                name: "Franchises");
        }
    }
}
